# NFS-Cluster with docker
## Objective
This docker project is to create a distributed system using Docker Swarm and NFS between different hosts.
## Description
There is a need for at least two different hosts for this system: Server side and Client side. An NFS server is to be mounted in a docker container to serve as a shared memory for different docker containers in different hosts. The access will be mounted through swarm overlay network.

## Set Up
This program is based on Linux/Unix operating system and therefore it is required to execute this program on Unix based OS. Also this program requires previous installation of Docker. 

### Server Side
A machine can be set as NFS server using the following command:  
`./run_nfs-server`

### Client Side
Client machines can connect to the Server host using the following command:  
`./build_deploy_nfs-client.sh <server-host_ip>`  
<server-host_ip> needs to be replaced by IP address of the server machine.

## Usage
The default shared folder will be `\nfs-client-volume`. To change this, one needs to modify the shell script `build_deploy_nfs-client.sh`.